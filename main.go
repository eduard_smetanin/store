package main

import (
	"fmt"
	"os"

	"bitbucket.org/eduard_smetanin/store/mail"
	"bitbucket.org/eduard_smetanin/store/orders"
	"bitbucket.org/eduard_smetanin/store/store"
)

func main() {
	notifCh := make(chan string)
	mail.ListenAndPrint(notifCh)
	orderService := store.New()
	orderService.Subscribe(notifCh)
	sale := orders.Sale{
		"apple":  {PurchasedCount: 2, ChargedCount: 1},
		"orange": {PurchasedCount: 3, ChargedCount: 2},
	}
	order := orders.New(os.Args[1:], sale)
	orderService.Submit(order)
	cost, err := order.Cost()
	if err != nil {
		fmt.Printf("error calculating the cost of %v: %v\n", order.Products, err)
		return
	}
	fmt.Printf("the cost of %v is $%.2f\n", order.Products, cost)
	fmt.Println("Press enter to exit")
	fmt.Scanln()
}
