package store

import (
	"fmt"
	"strings"

	"bitbucket.org/eduard_smetanin/store/orders"
)

// Store represents store for placing shopping orders.
type Store struct {
	Orders         []orders.Order
	NotificationCh chan string
}

// New creates a new store.
func New() Store {
	return Store{Orders: make([]orders.Order, 0)}
}

// Subscribe subscribes given channel for notifications.
func (s *Store) Subscribe(notificationCh chan string) {
	s.NotificationCh = notificationCh
}

// Submit places given order.
func (s *Store) Submit(order orders.Order) {
	if len(order.Products) > 5 { // Simulating low inventory.
		if s.NotificationCh == nil {
			return
		}
		s.NotificationCh <- fmt.Sprintf("order %v cannot be fulfilled because some items are out of stock.", order.Products)
		return
	}

	s.Orders = append(s.Orders, order)
	if s.NotificationCh == nil {
		return
	}
	s.NotificationCh <- fmt.Sprintf("order %v has been processed and is going to be delivered in 3 days.", order.Products)
}

// Exists checks if an order with the same products already exists.
func (s *Store) Exists(order orders.Order) bool {
	for _, o := range s.Orders {
		if equal(o.Products, order.Products) {
			return true
		}
	}
	return false
}

// equal tells whether a and b contain the same strings (case insensitive).
func equal(a, b []string) bool {
	if len(a) != len(b) {
		return false
	}
	for i, v := range a {
		if !strings.EqualFold(v, b[i]) {
			return false
		}
	}
	return true
}
