package orders_test

import (
	"errors"
	"testing"

	"bitbucket.org/eduard_smetanin/store/orders"
	"github.com/stretchr/testify/assert"
)

func TestCost(t *testing.T) {
	tests := []struct {
		products     []string
		sale         orders.Sale
		expectedCost float64
		expectedErr  error
	}{
		{
			products:     []string{"Apple", "Apple", "Orange", "Apple"},
			expectedCost: 2.05,
			expectedErr:  nil,
		},
		{
			products:     []string{"apple", "apple", "orange", "apple"},
			expectedCost: 2.05,
			expectedErr:  nil,
		},
		{
			products:     []string{"apple", "orange"},
			expectedCost: 0.85,
			expectedErr:  nil,
		},
		{
			products:     []string{},
			expectedCost: 0.0,
			expectedErr:  nil,
		},
		{
			products:     []string{"banana"},
			expectedCost: 0.0,
			expectedErr:  errors.New("unknown product banana"),
		},
		{
			products: []string{"Apple", "Apple", "Orange", "Apple"},
			sale: orders.Sale{
				"apple":  {PurchasedCount: 2, ChargedCount: 1},
				"orange": {PurchasedCount: 3, ChargedCount: 2},
			},
			expectedCost: 1.45,
			expectedErr:  nil,
		},
		{
			products: []string{"Apple", "Orange", "Orange", "Orange", "Orange"},
			sale: orders.Sale{
				"apple":  {PurchasedCount: 2, ChargedCount: 1},
				"orange": {PurchasedCount: 3, ChargedCount: 2},
			},
			expectedCost: 1.35,
			expectedErr:  nil,
		},
		{
			products: []string{"Apple", "Orange", "Orange", "Orange", "Orange", "Orange"},
			sale: orders.Sale{
				"apple":  {PurchasedCount: 2, ChargedCount: 1},
				"orange": {PurchasedCount: 3, ChargedCount: 2},
			},
			expectedCost: 1.60,
			expectedErr:  nil,
		},
		{
			products: []string{"Apple", "Orange", "Orange", "Orange", "Orange", "Orange", "Orange"},
			sale: orders.Sale{
				"apple":  {PurchasedCount: 2, ChargedCount: 1},
				"orange": {PurchasedCount: 3, ChargedCount: 2},
			},
			expectedCost: 1.60,
			expectedErr:  nil,
		},
		{
			products: []string{"Apple", "Orange"},
			sale: orders.Sale{
				"apple":  {PurchasedCount: 2, ChargedCount: 1},
				"orange": {PurchasedCount: 3, ChargedCount: 2},
			},
			expectedCost: 0.85,
			expectedErr:  nil,
		},
	}

	for _, test := range tests {
		order := orders.New(test.products, test.sale)
		cost, err := order.Cost()
		assert.Equal(t, test.expectedErr, err)
		assert.Equal(t, test.expectedCost, cost)
	}
}
