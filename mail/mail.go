package mail

import "fmt"

func ListenAndPrint(eventCh chan string) {
	go func() {
		for {
			select {
			case msg := <-eventCh:
				fmt.Println(msg)
			default:
				continue
			}
		}
	}()
}
