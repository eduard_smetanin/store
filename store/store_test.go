package store_test

import (
	"testing"

	"bitbucket.org/eduard_smetanin/store/orders"
	"bitbucket.org/eduard_smetanin/store/store"
	"github.com/stretchr/testify/assert"
)

func TestSubmit(t *testing.T) {
	tests := []struct {
		storeOrders []orders.Order
		orderToFind orders.Order
		orderExists bool
	}{
		{
			storeOrders: []orders.Order{
				orders.New([]string{"Apple", "Apple", "Orange", "Apple"}, orders.Sale{}),
				orders.New([]string{"Apple", "Orange"}, orders.Sale{}),
				orders.New([]string{}, orders.Sale{}),
			},
			orderToFind: orders.New([]string{"Apple", "Orange"}, orders.Sale{}),
			orderExists: true,
		},
		{
			storeOrders: []orders.Order{
				orders.New([]string{"Apple", "Apple", "Orange", "Apple"}, orders.Sale{}),
				orders.New([]string{"Apple", "Orange"}, orders.Sale{}),
				orders.New([]string{}, orders.Sale{}),
			},
			orderToFind: orders.New([]string{"Orange", "Apple"}, orders.Sale{}),
			orderExists: false,
		},
		{
			storeOrders: []orders.Order{
				orders.New([]string{"Apple", "Apple", "Orange", "Apple"}, orders.Sale{}),
				orders.New([]string{"Apple", "Orange"}, orders.Sale{}),
				orders.New([]string{}, orders.Sale{}),
			},
			orderToFind: orders.New([]string{}, orders.Sale{}),
			orderExists: true,
		},
		{
			storeOrders: []orders.Order{
				orders.New([]string{"APPLE"}, orders.Sale{}),
			},
			orderToFind: orders.New([]string{"apple"}, orders.Sale{}),
			orderExists: true,
		},
	}

	for _, test := range tests {
		srvc := store.New()
		for _, o := range test.storeOrders {
			srvc.Submit(o)
		}
		assert.Equal(t, test.orderExists, srvc.Exists(test.orderToFind))
	}
}

// TestSubscribe creates an orderService (store), subscribes to it's events,
// submits an order and ensures correct event is received.
func TestSubscribe(t *testing.T) {
	tests := []struct {
		storeOrder    orders.Order
		expectedNotif string
	}{
		{
			storeOrder:    orders.New([]string{"Apple", "Apple", "Orange", "Apple"}, orders.Sale{}),
			expectedNotif: "order [Apple Apple Orange Apple] has been processed and is going to be delivered in 3 days.",
		},
		{
			storeOrder:    orders.New([]string{"Apple", "Apple", "Orange", "Apple", "Apple", "Apple", "Orange", "Apple"}, orders.Sale{}),
			expectedNotif: "order [Apple Apple Orange Apple Apple Apple Orange Apple] has been cancelled because some items ran out of stock.",
		},
	}

	for _, test := range tests {
		doneCh := make(chan bool)

		notifCh := make(chan string)
		expectedNotif := test.expectedNotif
		go func() {
			for {
				select {
				case msg := <-notifCh:
					assert.Equal(t, expectedNotif, msg)
					doneCh <- true
				default:
					continue
				}
			}
		}()
	}
}
