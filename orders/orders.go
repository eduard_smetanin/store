package orders

import (
	"fmt"
	"strings"
)

var productPrice = map[string]float64{"apple": 0.60, "orange": 0.25}

type Sale map[string]struct{ PurchasedCount, ChargedCount int }

// Order represents store order.
type Order struct {
	Products []string
	Sale     Sale
}

// New creates a new order with given products.
func New(products []string, sale Sale) Order {
	return Order{
		Products: products,
		Sale:     sale,
	}
}

// Cost computes the total cost of the order's products.
func (o Order) Cost() (float64, error) {
	counts := productCounts(o.Products)
	sum := 0.0
	for lowecaseProduct, count := range counts {
		price, ok := productPrice[lowecaseProduct]
		if !ok {
			return 0.0, fmt.Errorf("unknown product %s", lowecaseProduct)
		}
		effectiveCount := count
		s, ok := o.Sale[lowecaseProduct]
		if ok {
			effectiveCount = count/s.PurchasedCount*s.ChargedCount + count%s.PurchasedCount
		}
		sum += price * float64(effectiveCount)
	}
	return sum, nil
}

// productCounts returns map where key is lowercased product name and key is number of times the product is included.
func productCounts(products []string) map[string]int {
	counts := make(map[string]int)
	for _, product := range products {
		lowecaseProduct := strings.ToLower(product)
		existingCount, ok := counts[lowecaseProduct]
		if ok {
			counts[lowecaseProduct] = existingCount + 1
		} else {
			counts[lowecaseProduct] = 1
		}
	}
	return counts
}
